import logging
import webbrowser
import PySimpleGUI as sg

from src.constants import VERSION, Colors, PATTERN
from src.functions import change_frame_color, change_frame_text, get_html_from_request, get_video_url, \
    get_clipboard_content, override_clipboard_on_success
from src.settings import SETTINGS, logo

logging.basicConfig(format='%(levelname)s | %(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p | ',
                    filename=SETTINGS.log_file, level=SETTINGS.logging)

logging.info(f'Application starting version {VERSION}')
logging.info(f'Pattern: {PATTERN}')

# Theme (change in constants)
sg.theme(SETTINGS.theme)

# Persistent data
html = None

# Layout
layout = [
    [sg.Frame('', layout=[
        [sg.Text('URL odkaz na video'), sg.Push(), sg.Exit('Konec'),],
        [sg.Input(key='url', do_not_clear=False)],
        [sg.Button('Výběr videa', key='open_url'), sg.Button('Spustit video', key="Otevřít"), sg.Push(), sg.Checkbox('Čmuchadlo', default=SETTINGS.allow_sniffer, disabled=True), sg.Text(f'Verze {VERSION}')]
    ], element_justification='center', title_location='n', key='frame1')],
]

# Window
window = sg.Window('Přehraj to!', layout, finalize=True, keep_on_top=True, icon="./assets/icon.png")

# Shortcuts
window.bind("<Return>", "open")
window.bind("<Escape>", "minimalize")

# Elements
frame_element = window['frame1']
url_element = window['url']

# Event loop
while True:
    event, values = window.read(timeout=3000)

    if event == sg.TIMEOUT_KEY:
        url = get_clipboard_content()
        if not url:
            continue
        resp = get_html_from_request(url)
        if resp['success']:
            logging.info(f'[Sniffer] 200 OK for "{url}"')
            change_frame_color(frame_element, Colors.green.value)
            change_frame_text(frame_element, f"{resp['msg']} [{resp['code']}]")
            html = resp['html']
            logging.info(f'[Sniffer] Getting S3 URL for "{url}"')
            video_url = get_video_url(html)
            if video_url:
                logging.info(f'[Sniffer] Found usable URL "{video_url}" (last 2 chars will be removed)')
                tmp = webbrowser.open(video_url[:-2])
                if tmp:
                    logging.info(f'[Sniffer] Routine passed with no issues (minimalizing)')
                    change_frame_text(frame_element, 'Video detekováno')
                    window.minimize()
                    override_clipboard_on_success()
            else:
                logging.error(f'[Sniffer] Pattern not found in HTML content')
                logging.error(f'[Sniffer] Routine failed for "{values['url']}"')
                logging.debug(f'[Sniffer] HTML: {html}')
            html = None
        else:
            logging.error(f'[Sniffer] {resp['code']} for "{values['url']}"')
            logging.error(f'[Sniffer] Routine failed for "{values['url']}"')
            html = None

    # Exit Case
    if event == sg.WIN_CLOSED or event == 'Konec':
        logging.info('Application closed')
        break

    if event == 'minimalize':
        window.minimize()

    if event == 'open_url':
        webbrowser.open(SETTINGS.prehraj_to_url)

    # Run case
    if event == 'Otevřít' or event == 'open':
        logging.info('Open event triggered')
        if values['url']:
            logging.info(f'Creating GET request for "{values['url']}"')
            resp = get_html_from_request(values['url'])
            if resp['success']:
                logging.info(f'200 OK for "{values['url']}"')
                change_frame_color(frame_element, Colors.green.value)
                change_frame_text(frame_element, f"{resp['msg']} [{resp['code']}]")
                html = resp['html']
                logging.info(f'Getting S3 URL for "{values['url']}"')
                video_url = get_video_url(html)
                if video_url:
                    logging.info(f'Found usable URL "{video_url}" (last 2 chars will be removed)')
                    tmp = webbrowser.open(video_url[:-2])
                    if tmp:
                        logging.info(f'Routine passed with no issues (minimalizing)')
                        change_frame_text(frame_element, 'Video spuštěno')
                        window.minimize()
                else:
                    logging.error(f'Pattern not found in HTML content')
                    logging.error(f'Routine failed for "{values['url']}"')
                    logging.debug(f'HTML: {html}')
                    change_frame_color(frame_element, Colors.red.value)
                    change_frame_text(frame_element, 'Odkaz na video nebyl získán pomocí vzorce')
                html = None
            else:
                logging.error(f'{resp['code']} for "{values['url']}"')
                logging.error(f'Routine failed for "{values['url']}"')
                change_frame_color(frame_element, Colors.red.value)
                change_frame_text(frame_element, f"{resp['error']} [{resp['code']}]")
                html = None
        # Url empty
        else:
            logging.warning(f'Invalid value in URL field ({values['url']})')
            html = None
            change_frame_color(frame_element, Colors.red.value)
            change_frame_text(frame_element, 'Neplatná hodnota odkazu')

window.close()
