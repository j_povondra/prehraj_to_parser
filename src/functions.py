import logging
import re
import requests
from requests.exceptions import InvalidURL, ConnectionError, MissingSchema
import pyperclip

from src.constants import PATTERN


def get_html_from_request(url):
    resp = {}

    try:
        request = requests.get(url)
        if request.status_code == 200:
            resp['success'] = True
            resp['msg'] = "OK"
            resp['code'] = request.status_code
            resp['html'] = request.text
        elif request.status_code == 404:
            resp['success'] = False
            resp['error'] = "Stránka nenalezena"
            resp['code'] = request.status_code
        elif request.status_code == 500:
            resp['success'] = False
            resp['error'] = "Chyba na straně serveru"
            resp['code'] = request.status_code
        else:
            resp['success'] = False
            resp['error'] = "Neznámá chyba"
            resp['code'] = request.status_code

    except InvalidURL:
        resp['success'] = False
        resp['error'] = "Chybná URL"
        resp['code'] = 'INVALID_URL'

    except ConnectionError:
        resp['success'] = False
        resp['error'] = "Chybná doména"
        resp['code'] = 'INVALID_DOMAIN'

    except MissingSchema:
        resp['success'] = False
        resp['error'] = "Chybná URL"
        resp['code'] = 'INVALID_URL'

    return resp


def get_video_url(html):
    pattern = re.compile(PATTERN)

    matches = pattern.findall(html)
    logging.debug(f'Pattern matches: {matches}')

    list_of_matches = []

    for match in matches:
        if '.mp4' in match:
            list_of_matches.append(match)
    logging.debug(f'MP4 list matches: {list_of_matches}')
    return list_of_matches[-1] if list_of_matches else None


def change_frame_color(frame_element, new_color):
    frame_element.Widget.config(bg=new_color)


def change_frame_text(frame_element, new_text):
    frame_element.update(new_text)


def get_clipboard_content():
    clipboard_content = pyperclip.paste()

    if 'prehrajto.cz' not in clipboard_content:
        return False
    else:
        return clipboard_content


def override_clipboard_on_success():
    pyperclip.copy('')
