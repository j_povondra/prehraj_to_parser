import logging
from enum import Enum

VERSION = 0.3
THEME = 'DarkTanBlue'

LOG_LEVEL = logging.INFO
LOG_FILENAME = 'prehraj_to_parser.log'

PREHRAJTO_URL = 'https://prehrajto.cz/'

PATTERN = r'https://fstorage[0-9]+\.premiumcdn\.net/[^\s]+'

class Colors(Enum):
    green = "#4bb543"
    red = "#f94449"
    blue = "#3e88ef"
